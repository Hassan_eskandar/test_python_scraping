
#Exercise 1
import requests

def http_request(url):
    URL = 'https://httpbin.org/anything'
	PARAMS = {'isadmin': 1}
	response = requests.get(url = URL, params=PARAMS)
	#r.headers
	#r.content
	response = response.text
	return response


#Exercise 2
import json 
def get_available_products(data):
	list_of_available_prods = []

	for product in data['Bundles']:

        #fetch product name, if no name is found then skip the product
    	if 'Name' in product.keys():
        	name = product['Name']  
    	else:
	        continue 
        
        #extract product information, the product key in the dict could be case sensitive
        #if not product info found, log it as missing info
	    if 'Product' in product.keys():
	        prd_key = 'Product'  
	    elif 'Products' in product.keys():
	        prd_key = 'Products'
	    else:
	        print('Some information about product {} is missing'.format(name))
	        continue
	    prod_info_list = product[prd_key]

	    #loop over the product info to find the dict that contains the
	    #price/availability/barcode(product_ID) data and store it
	    for j in range(len(prod_info_list)):
	        if 'IsAvailable' in prod_info_list[j].keys():
	            isAvailable = prod_info_list[j]['IsAvailable']
	            price = prod_info_list[j]['Price']
	            barcode = prod_info_list[j]['Barcode']
	            break

	    #if a product is available print a statement
	    #add it to the list of available products     
        #else log its unavailability zith the corresponding ID
	    if isAvailable:
	        list_of_available_prods.append(product)
	        print('You can buy {} at our store at {}'.format(name[:30], round(price,1)))
	    else:
	        print('Product {} with barcode ID {} is unvailable'.format(name[:30],barcode))

    return list_of_available_prods

def save_prods_tocsv(list_prods):
	import csv
	with open('available_prods.csv', 'w', newline='') as f:
    	wr = csv.writer(f)
    	wr.writerows(list_prods)

def save_prods_tocsv_df(list_prods):
	import pandas as pd
    df = pd.DataFrame.from_records(list_prods)
	df.to_csv(r'df_data.csv')
