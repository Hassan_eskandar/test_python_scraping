#Exercise 1
def exercise_one():
	for i in range(1,101):
	    if i%3==0 and i%5==0:
	        print('ThreeFive')
	    elif i%3==0:
	        print('Three')
	    elif i%5==0:
	        print('Five')
	    else:
	        print(i)
	pass

#Exercise 2 
def find_missing_nb(l):
    return [x for x in range(l[0], l[-1]+1) if x not in l]
    pass

#Exercise 3
def calculate(arr):
    if type(arr) is not list: return False
    total = 0
    for i in arr:
        try:
            if isinstance(i, str):
                total = total + int(i)
        except ValueError:
            continue
    return total
    
#Exercise 4
def sortArray(a, n): 
    
    # Store all non-negative values
    list1=[] 
    for i in range(n): 
        if (a[i] >= 0): 
            list1.append(a[i]) 
  
    list1 = sorted(list1) 
    
    # If current element is non-negative then 
    # update it such that all the non-negative values are sorted
    j = 0
    for i in range(n): 

        if (a[i] >= 0): 
            a[i] = list1[j] 
            j += 1
  
    for i in range(n): 
        print(a[i],end = " ") 
    
    return a
