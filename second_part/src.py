
class MySet(set):

    # todo exercise 1
    def __init__(self,data):
        self.data = set(data)
    def __add__(self, x):
        return self.data.union(x.data)
    def __sub__(self, x):
        return self.data.difference(x.data)


def decorator_check_max_int(func):
    # todo exercise 2
    def wrapper():
        from sys import maxsize
        f = func()
        if f > maxsize:
            return maxsize
        return f
    return wrapper


@decorator_check_max_int
def add(a, b):
    return a + b


def ignore_exception(exception):
    try:
        pass
    except exception:
        return None
    # todo exercise 3
    return lambda x: x


@ignore_exception(ZeroDivisionError)
def div(a, b):
    return a / b


@ignore_exception(TypeError)
def raise_something(exception):
    raise exception


# exercise 4
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap


class MetaInherList(list):
    # todo exercise 5
    def __init__(self,l):
        super().__init__(l)


class Ex:
    x = 4


class ForceToList(Ex):
    __metaclass__=MetaInherList
    pass


#exercise 6 solution
class MetaProcessCheck():
    def __call__(self, searchClass):
        from inspect import signature
        if hasattr(searchClass, "process") and len(signature(searchClass.process).parameters)==3:
            return True
        else:
            return False

